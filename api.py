#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bottle import get, post, run, response, request
import sqlite3
import json

HOST = 'localhost'
PORT = 8888

conn = sqlite3.connect("cookies.db")


def format_response(d):
    return json.dumps(d, indent=4) + "\n"

def enough_ingredients(recipe):
    c = conn.cursor()
    c.execute(
        """
        WITH needed AS (
            SELECT name, amount AS Qper100
            FROM ingredients
            WHERE recipe_name = ?
        ), have AS (
            SELECT name, sum(amount) AS quantity
            FROM raw_material_transactions
            GROUP BY name
        )
        SELECT name, (quantity - Qper100 * 54) as saldo
        FROM needed
        JOIN have
        USING (name)
        WHERE saldo < 0;
        """,
        [recipe]
    )

    s = [{"ingredient": name, "quantity": saldo}
        for (name, saldo) in c]

    return len(s) == 0

    #return all([checkIngredient(t, i) for t in s])

def cookie_exists(name):
    c = conn.cursor()
    c.execute(
        """
        SELECT name
        FROM   recipes
        WHERE  name = ?
        """,
        [name]
    )
    return c.fetchone() is not None

def use_ingredients(recipe):
    c = conn.cursor()
    c.execute(
        """
        SELECT name, amount AS Qper100, unit
        FROM ingredients
        WHERE recipe_name = ?
        """,
        [recipe]
    )
    s = [{"ingredient": name, "amount": Qper100, "unit": unit}
        for(name, Qper100, unit) in c]
    query = """
        INSERT
        INTO raw_material_transactions (name, amount, unit)
        VALUES
        """ + ', '.join([f"(\'{z['ingredient']}\', {z['amount']}*-54, \'{z['unit']}\')" for z in s]) + ';'
    c.execute(query)
    conn.commit()

@post('/reset')
def post_reset():
   response.content_type = 'application/json'
   c = conn.cursor()
   file = open("initial-data.sql","r")
   string = file.read()
   c.executescript(string)
   conn.commit()
   response.status = 200
   return format_response({"status":"ok"})


@get('/customers')
def get_customers():
    response.content_type = 'application/json'
    query =    """
        SELECT name, address
        FROM   customers
        """
    c = conn.cursor()
    c.execute(
        query
    )
    s = [{"name": name, "address": address}
        for (name, address) in c]
    return format_response({"customers": s})


@get('/ingredients')
def get_ingredients():
    response.content_type = 'application/json'
    query =    """
        SELECT name, sum(amount) AS quantity, unit
        FROM raw_material_transactions
        GROUP BY name
        ORDER BY name;
        """
    c = conn.cursor()
    c.execute(
        query
    )
    s = [{"name": name, "quantity": quantity, "unit": unit}
        for (name, quantity, unit) in c]
    return format_response({"ingredients": s})

@get('/cookies')
def get_cookies():
    response.content_type = 'application/json'
    query =    """
        SELECT DISTINCT name
        FROM recipes
        ORDER BY name;
        """
    c = conn.cursor()
    c.execute(
        query
    )
    s = [{"name": name[0]}
        for name in c]
    return format_response({"cookies": s})

@get('/recipes')
def get_recipes():
    response.content_type = 'application/json'
    query =    """
        SELECT recipe_name, name, amount, unit
        FROM ingredients
        ORDER BY recipe_name, name;
        """
    c = conn.cursor()
    c.execute(
        query
    )
    s = [{"cookie": recipe_name, "ingredient": name, "quantity": amount, "unit": unit}
        for (recipe_name, name, amount, unit) in c]
    return format_response({"recipes": s})

@post('/pallets')
def post_pallet():
    response.content_type = 'application/json'
    query =    """
        INSERT
        INTO pallets (recipe_name)
        VALUES (?);
        """
    params = []

    if request.query.cookie:
        if cookie_exists(request.query.cookie):
            if enough_ingredients(request.query.cookie):
                params.append(request.query.cookie)
                use_ingredients(request.query.cookie)
            else:
               response.status = 400
               return format_response({"status":"not enough ingredients"})
        else:
            response.status = 400
            return format_response({"status":"no such cookie"})
    else:
        response.status = 400
        return format_response({"status":"missing parameters"})

    c = conn.cursor()
    c.execute(
        query,
        params
    )
    conn.commit()
    c.execute(
        """
        SELECT   pallet_id
        FROM     pallets
        WHERE    rowid = last_insert_rowid()
        """
    )
    response.status = 200
    return format_response({"status":"ok", "id": c.fetchone()[0]})

@get('/pallets')
def get_pallets():
    response.content_type = 'application/json'
    query =    """
        SELECT pallet_id, recipe_name, production_date, customers.name AS customer_name, is_blocked
        FROM pallets
        LEFT JOIN orders
        USING (order_id)
        LEFT JOIN customers
        USING (customer_id)
        WHERE 1=1
        """
    params = []
    if request.query.after:
        query += "AND production_date > ?"
        params.append(request.query.after)
    if request.query.before:
        query += "AND production_date < ?"
        params.append(request.query.before)
    if request.query.cookie:
        query += "AND recipe_name = ?"
        params.append(request.query.cookie)
    if request.query.blocked:
        query += "AND is_blocked = ?"
        params.append(request.query.blocked)

    c = conn.cursor()
    c.execute(
        query,
        params
    )
    conn.commit()
    s = [{"id": pallet_id, "cookie": recipe_name, "productionDate": production_date, "customer": customer_name, "blocked": is_blocked}
        for (pallet_id, recipe_name, production_date, customer_name, is_blocked) in c]
    return format_response({"pallets": s})

@post('/block/<cookie_name>/<from_date>/<to_date>')
def post_block(cookie_name, from_date, to_date):
    response.content_type = 'application/json'
    params = []
    query = """
        UPDATE pallets
        SET is_blocked = 1
        WHERE recipe_name = ?
        AND   production_date >= ?
        AND   production_date <= ?
        """

    params.append(cookie_name);
    params.append(from_date);
    params.append(to_date);

    c = conn.cursor()
    c.execute(
        query,
        params
    )
    conn.commit()

    response.status = 200
    return format_response({"status":"ok"})

@post('/unblock/<cookie_name>/<from_date>/<to_date>')
def post_unblock(cookie_name, from_date, to_date):
    response.content_type = 'application/json'
    params = []
    query = """
        UPDATE pallets
        SET is_blocked = 0
        WHERE recipe_name = ?
        AND   production_date >= ?
        AND   production_date <= ?
        """

    params.append(cookie_name);
    params.append(from_date);
    params.append(to_date);

    c = conn.cursor()
    c.execute(
        query,
        params
    )
    conn.commit()

    response.status = 200
    return format_response({"status":"ok"})

run(host=HOST, port=PORT, debug=True)
