# EDAF75, project report

This is the report for

 + Viktor Claesson, `dat15vcl`
 + Simon Rydebrink, `dat14sry`
 + Axel Syrén, `dat15asy`

We solved this project on our own, except for:

 + The Peer-review meeting


## ER-design

The model is in the file [`UML.png`](UML.png):

<center>
    <img src="UML.png" width="559p">
</center>


## Relations

The ER-model above gives the following relations (neither
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nor [HTML5](https://en.wikipedia.org/wiki/HTML5) handles
underlining withtout resorting to
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
so we use bold face for primary keys, italicized face for
foreign keys, and bold italicized face for attributes which
are both primary keys and foreign keys):

+ recipes(**name**, active)
+ ingredients(**_recipe_name_**, **name**, amount, unit)
+ customers(**customer_id**, name, address)
+ orders(**order_id**, _customer_id_, to_be_delivered)
+ pallets(**pallet_id**, status, production_date, delivery_time, is_blocked, _order_id_, _recipe_name_)
+ order_contents (**_order_id_**, **_recipe_name_**, nbr)
+ raw_material_transactions(**name**, amount, unit, **time**)


## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`create-schema.sql`](create-schema.sql) (defines the tables), and
 + [`initial-data.sql`](initial-data.sql) (inserts data).

So, to create and initialize the database, we run:

```shell
sqlite3 cookies.db < create-schema.sql
sqlite3 cookies.db < initial-data.sql
```

## How to compile and run the program

This section should give a few simple commands to type to
compile and run the program from the command line, such as:

```shell
python api.py
```

Note that you need Python 3.6.