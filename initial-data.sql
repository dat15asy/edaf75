DELETE FROM recipes;
DELETE FROM ingredients;
DELETE FROM customers;
DELETE FROM orders;
DELETE FROM pallets;
DELETE FROM order_contents;
DELETE FROM raw_material_transactions;

INSERT
INTO   recipes(name)
VALUES ('Tango'),
       ('Nut ring'),
       ('Amneris'),
       ('Nut cookie'),
       ('Almond delight'),
       ('Berliner');


INSERT
INTO   ingredients(name, amount, unit, recipe_name)
VALUES ('Butter', 200, 'g', 'Tango'),
       ('Sugar', 250, 'g', 'Tango'),
       ('Flour', 300, 'g', 'Tango'),
       ('Sodium bicarbonate', 4, 'g', 'Tango'),
       ('Vanilla', 2, 'g', 'Tango'),
       ('Flour', 450, 'g', 'Nut ring'),
       ('Butter', 450, 'g', 'Nut ring'),
       ('Icing sugar', 190, 'g', 'Nut ring'),
       ('Roasted chopped nuts', 225, 'g', 'Nut ring'),
       ('Fine-ground nuts', 750, 'g', 'Nut cookie'),
       ('Ground, roasted nuts', 625, 'g', 'Nut cookie'),
       ('Bread crumbs', 125, 'g', 'Nut cookie'),
       ('Sugar', 375, 'g', 'Nut cookie'),
       ('Egg whites', 350, 'ml', 'Nut cookie'),
       ('Chocolate', 50, 'g', 'Nut cookie'),
       ('Marzipan', 750, 'g', 'Amneris'),
       ('Butter', 250, 'g', 'Amneris'),
       ('Eggs', 250, 'g', 'Amneris'),
       ('Potato starch', 25, 'g', 'Amneris'),
       ('Wheat flour', 25, 'g', 'Amneris'),
       ('Butter', 400, 'g', 'Almond delight'),
       ('Sugar', 270, 'g', 'Almond delight'),
       ('Chopped almonds', 279, 'g', 'Almond delight'),
       ('Flour', 400, 'g', 'Almond delight'),
       ('Cinnamon', 10, 'g', 'Almond delight'),
       ('Flour', 350, 'g', 'Berliner'),
       ('Butter', 250, 'g', 'Berliner'),
       ('Icing sugar', 100, 'g', 'Berliner'),
       ('Eggs', 50, 'g', 'Berliner'),
       ('Vanilla sugar', 5, 'g', 'Berliner'),
       ('Chocolate', 50, 'g', 'Berliner');

INSERT
INTO   customers(customer_id, name, address)
VALUES (1, 'Finkakor AB', 'Helsingborg'),
       (2, 'Småkakor AB', 'Malmö'),
       (3, 'Kaffebröd AB', 'Landskrona'),
       (4, 'Bjudkakor AB', 'Ystad'),
       (5, 'Kalaskakor AB', 'Trelleborg'),
       (6, 'Partykakor AB', 'Kristianstad'),
       (7, 'Gästkakor AB', 'Hässleholm'),
       (8, 'Skånekakor AB', 'Perstorp');

INSERT
INTO   raw_material_transactions(name, unit, amount)
VALUES ('Flour', 'g', 100000),
       ('Butter', 'g', 100000),
       ('Icing sugar', 'g', 100000),
       ('Roasted, chopped nuts', 'g', 100000),
       ('Fine-ground nuts', 'g', 100000),
       ('Ground, roasted nuts', 'g', 100000),
       ('Bread crumbs', 'g', 100000),
       ('Sugar', 'g', 100000),
       ('Egg whites', 'ml', 100000),
       ('Chocolate', 'g', 100000),
       ('Marzipan', 'g', 100000),
       ('Eggs', 'g', 100000),
       ('Potato starch', 'g', 100000),
       ('Wheat flour', 'g', 100000),
       ('Sodium bicarbonate', 'g', 100000),
       ('Vanilla', 'g', 100000),
       ('Chopped almonds', 'g', 100000),
       ('Cinnamon', 'g', 100000),
       ('Vanilla sugar', 'g', 100000);
